# ES6 Workshops

## Workshops Goal

This ES6 workshops intends to help developers learning all the newest ES6 syntax and all latest syntax constructs in JavaScript.

## Your Goal

The `exercises` folder contains a set of failing tests. For each test, you will need to write code using the latest ES6 syntax. Once all of your tests pass on an exercise, you will want to move onto the next one.

## Requirements

* [Git](https://git-scm.com/) or [Git for Windows](https://gitforwindows.org/)
* [Node.js](https://nodejs.org/en/)
* [Yarn](https://yarnpkg.com/lang/en/)

## Getting Started

### Install dependencies

```shell
yarn
```

### How To Run Tests

To run all the test suite:

```shell
yarn test
```

To run a specific test file:

```shell
yarn test -- exercises/01-intro.js
```

To run the updated tests as you make changes:

```shell
yarn test:watch
```

## Code of Conduct

[Code of Conduct](./CODE_OF_CONDUCT.md)

## Contributing

[Contributing](./CONTRIBUTING.md)

### How to add workshops

To add worshop:

```shell
$ hygen exercise add
? Exercise name? modules
? Exercise number? 02

Loaded templates: _templates
       added: exercises/-modules.js
```

## License

[MIT License](./LICENSE.md)
