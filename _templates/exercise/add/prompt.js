module.exports = [
  {
    type: 'input',
    name: 'name',
    message: 'Exercise name?',
  },
  {
    type: 'input',
    name: 'number',
    message: 'Exercise number?',
  },
];
